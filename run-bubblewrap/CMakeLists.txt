add_executable(buildbox-run-bubblewrap
    buildboxrun_bubblewrap.cpp
    buildboxrun_bubblewrap.m.cpp
)
target_precompile_headers(buildbox-run-bubblewrap REUSE_FROM common)

install(TARGETS buildbox-run-bubblewrap RUNTIME DESTINATION bin)
target_link_libraries(buildbox-run-bubblewrap common)
target_include_directories(buildbox-run-bubblewrap PRIVATE ".")

include(CTest)
if(BUILD_TESTING)
    add_subdirectory(test)
endif()
