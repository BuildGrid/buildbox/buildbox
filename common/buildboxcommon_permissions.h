/*
 * Copyright 2024 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCOMMON_PERMISSIONS_H
#define INCLUDED_BUILDBOXCOMMON_PERMISSIONS_H

#include <sys/types.h>

constexpr mode_t PERMISSION_XUSR = S_IXUSR;                     // 0100
constexpr mode_t PERMISSION_XALL = S_IXUSR | S_IXGRP | S_IXOTH; // 0111
constexpr mode_t PERMISSION_RWUSR = S_IRUSR | S_IWUSR;          // 0600
constexpr mode_t PERMISSION_RWUSR_RALL =
    S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH; // 0644
constexpr mode_t PERMISSION_RWALL =
    S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH; // 0666
constexpr mode_t PERMISSION_RWXUSR = S_IRWXU;                  // 0700
constexpr mode_t PERMISSION_RWXUSR_RXALL = S_IRUSR | S_IWUSR | S_IXUSR |
                                           S_IRGRP | S_IXGRP | S_IROTH |
                                           S_IXOTH; // 0755
constexpr mode_t PERMISSION_RWXALL = S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP |
                                     S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH |
                                     S_IXOTH; // 0777
constexpr mode_t PERMISSION_RWXALL_SPECIAL =
    S_ISUID | S_ISGID | S_ISVTX | S_IRUSR | S_IWUSR | S_IXUSR | S_IRGRP |
    S_IWGRP | S_IXGRP | S_IROTH | S_IWOTH | S_IXOTH; // 07777

#endif
