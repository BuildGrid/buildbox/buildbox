// Copyright 2025 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INCLUDED_BUILDBOXCOMMON_METRICNAMES_H
#define INCLUDED_BUILDBOXCOMMON_METRICNAMES_H

#include <string>

namespace buildboxcommon {

class CommonMetricNames {
  private:
    CommonMetricNames() = delete;

  public:
    // Structure to contain the static statsd strings under which metrics are
    // published. Refer to buildboxcommon_metricnames.cpp to determine the
    // definition of the metric and the associated statsd name.

    /*
     * LRU metrics
     */
    static const std::string GAUGE_NAME_DISK_USAGE;
    static const std::string TIMER_NAME_LRU_CLEANUP;
    static const std::string GAUGE_NAME_LRU_EFFECTIVE_HIGH_WATERMARK;
    static const std::string GAUGE_NAME_LRU_EFFECTIVE_LOW_WATERMARK;
    static const std::string GAUGE_NAME_LRU_CONFIGURED_HIGH_WATERMARK;
    static const std::string GAUGE_NAME_LRU_LOW_WATERMARK_PERCENTAGE;

    /*
     * LOCAL CAS
     */
    static const std::string COUNTER_NAME_BYTES_READ;
    static const std::string COUNTER_NAME_BYTES_WRITE;
    static const std::string COUNTER_NUM_BLOBS_READ_FROM_LOCAL;
    static const std::string COUNTER_NUM_BLOBS_WRITTEN_TO_LOCAL;

    // Stage
    static const std::string COUNTER_NUM_HARDLINK_STAGER_STAGED_FILES;
    static const std::string COUNTER_NUM_HARDLINK_STAGER_STAGED_DIRECTORIES;
    static const std::string COUNTER_NUM_HARDLINK_STAGER_STAGED_SYMLINKS;
};

} // namespace buildboxcommon

#endif // INCLUDED_BUILDBOXCOMMON_METRICNAMES_H
