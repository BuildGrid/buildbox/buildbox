// Copyright 2025 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <buildboxcommon_metricnames.h>

namespace buildboxcommon {
// Each metric is recorded in code with a statsd string. The statsd strings for
// each named metric are defined here. Only the constants are used in code, not
// the metric strings directly.

// LRU
// Total bytes on local disk being consumed by blobs
const std::string CommonMetricNames::GAUGE_NAME_DISK_USAGE =
    "localcas_disk_usage";

// Elapsed wall time of disk cleanup operations
const std::string CommonMetricNames::TIMER_NAME_LRU_CLEANUP =
    "localcas_lru_cleanup";

// Runtime-calculated high watermark in bytes
const std::string CommonMetricNames::GAUGE_NAME_LRU_EFFECTIVE_HIGH_WATERMARK =
    "localcas_lru_effective_highwatermark";

// Runtime-calculated low watermark in bytes
const std::string CommonMetricNames::GAUGE_NAME_LRU_EFFECTIVE_LOW_WATERMARK =
    "localcas_lru_effective_lowwatermark";

// Configured high watermark in bytes
const std::string CommonMetricNames::GAUGE_NAME_LRU_CONFIGURED_HIGH_WATERMARK =
    "localcas_lru_configured_highwatermark";

// Configured low watermark as a percentage relative to the high watermark
const std::string CommonMetricNames::GAUGE_NAME_LRU_LOW_WATERMARK_PERCENTAGE =
    "localcas_lru_lowwatermark_percentage";

// LOCAL CAS
// Running total of number of blobs read from local storage
const std::string CommonMetricNames::COUNTER_NUM_BLOBS_READ_FROM_LOCAL =
    "localcas_num_blobs_read_from_local";

// Number of bytes read from casd's local CAS storage
const std::string CommonMetricNames::COUNTER_NAME_BYTES_READ =
    "localcas_bytes_read";

// Number of bytes written to casd's local CAS storage
const std::string CommonMetricNames::COUNTER_NAME_BYTES_WRITE =
    "localcas_bytes_write";

// Running total of number of blobs read from local storage
const std::string CommonMetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_LOCAL =
    "localcas_num_blobs_written_to_local";

// Stage
// Number of files staged
const std::string CommonMetricNames::COUNTER_NUM_HARDLINK_STAGER_STAGED_FILES =
    "localcas_num_hardlink_stager_files_staged";

// Number of directories staged
const std::string
    CommonMetricNames::COUNTER_NUM_HARDLINK_STAGER_STAGED_DIRECTORIES =
        "localcas_num_hardlink_stager_directories_staged";

// Number of symlinks staged
const std::string
    CommonMetricNames::COUNTER_NUM_HARDLINK_STAGER_STAGED_SYMLINKS =
        "localcas_num_hardlink_stager_symlinks_staged";

} // namespace buildboxcommon
