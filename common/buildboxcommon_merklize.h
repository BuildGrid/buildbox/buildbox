// Copyright 2019 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef INCLUDED_BUILDBOXCOMMON_MERKLIZE
#define INCLUDED_BUILDBOXCOMMON_MERKLIZE

#include <buildboxcommon_protos.h>
#include <buildboxcommon_timeutils.h>
#include <ThreadPool.h>

#include <google/protobuf/message.h>
#include <string>
#include <unordered_map>
#include <vector>

namespace buildboxcommon {

typedef std::unordered_map<buildboxcommon::Digest, std::string>
    digest_string_map;

typedef std::unordered_map<buildboxcommon::Digest, std::string>::iterator
    digest_string_map_it;

typedef std::function<Digest(int fd)> FileDigestFunction;

typedef std::function<mode_t(mode_t mode)> UnixModeUpdater;

// Create a UnixModeUpdater that applies a given mask
UnixModeUpdater unixModeMaskUpdater(mode_t mask);

/**
 * Represents a single file.
 */
struct File {
    Digest d_digest;
    bool d_executable = false;
    NodeProperties d_nodeProperties;

    File(){};

    File(Digest digest, bool executable, const NodeProperties &nodeProperties)
        : d_digest(digest), d_executable(executable),
          d_nodeProperties(nodeProperties){};

    /**
     * Constructs a File given the path to a file on disk.
     */
    File(const char *path,
         const std::vector<std::string> &capture_properties = {},
         const UnixModeUpdater &unixModeUpdater = {},
         const std::map<std::string, std::string> &nodeProperties = {});
    File(const char *path, const FileDigestFunction &fileDigestFunc,
         const std::vector<std::string> &capture_properties = {},
         const UnixModeUpdater &unixModeUpdater = {},
         const std::map<std::string, std::string> &nodeProperties = {});
    File(int dirfd, const char *path, const FileDigestFunction &fileDigestFunc,
         const std::vector<std::string> &capture_properties = {},
         const UnixModeUpdater &unixModeUpdater = {},
         const std::map<std::string, std::string> &nodeProperties = {});
    File(int fd, const std::vector<std::string> &capture_properties = {},
         const UnixModeUpdater &unixModeUpdater = {},
         const std::map<std::string, std::string> &nodeProperties = {});
    File(int fd, const FileDigestFunction &fileDigestFunc,
         const std::vector<std::string> &capture_properties = {},
         const UnixModeUpdater &unixModeUpdater = {},
         const std::map<std::string, std::string> &nodeProperties = {});

    bool operator==(const File &other) const
    {
        return d_digest == other.d_digest &&
               d_executable == other.d_executable &&
               google::protobuf::util::MessageDifferencer::Equals(
                   d_nodeProperties, other.d_nodeProperties);
    }

    /**
     * Converts a File to a FileNode with the given name.
     */
    FileNode to_filenode(const std::string &name) const;

  private:
    void init(int fd, const FileDigestFunction &fileDigestFunc,
              const std::vector<std::string> &capture_properties = {},
              const UnixModeUpdater &unixModeUpdater = {},
              const std::map<std::string, std::string> &nodeProperties = {});
};

/**
 * Represent an ignore pattern with the same format as fnmatch
 */
struct IgnorePattern {
    IgnorePattern(const std::string &pattern, const bool matchBasenameOnly,
                  const bool matchDirectoryOnly)
        : d_pattern(pattern), d_matchBasenameOnly(matchBasenameOnly),
          d_matchDirectoryOnly(matchDirectoryOnly)
    {
    }

    bool operator==(const IgnorePattern &other) const;

    /**
     * Parse an IgnorePattern form a string.
     *
     * Beginning slashes will be trimmed so patterns are relative to
     * a path prefix.
     */
    static IgnorePattern fromString(const std::string &s);

    std::string d_pattern;

    // If this flag is true, the matching only consider the basename
    // while ignoring the directory path.
    // For example, `*.o` match with `dir1/dir2/a.o`
    bool d_matchBasenameOnly;

    // Only match directory
    bool d_matchDirectoryOnly;
};

std::ostream &operator<<(std::ostream &o, const IgnorePattern &ignorePattern);

/**
 * Represent a collection of IgnorePatterns and the
 * path prefix the patterns should work within
 */
class IgnoreMatcher {
  public:
    /**
     * Parse a vector of ignore patterns.
     *
     * The result is a shared pointer because patterns might be shared
     * between multiple matchers.
     */
    static std::shared_ptr<std::vector<IgnorePattern>>
    parseIgnorePatterns(std::istream &is);
    static std::shared_ptr<std::vector<IgnorePattern>>
    parseIgnorePatterns(const std::vector<std::string> &patterns);

    /**
     * Construct `IgnoreMatcher` from a path prefix which can be empty,
     * and a vector of fnmatch patterns relative to it
     */
    IgnoreMatcher(
        const std::string &pathPrefix,
        const std::shared_ptr<std::vector<IgnorePattern>> &ignorePatterns);

    bool operator==(const IgnoreMatcher &other) const;

    /**
     * Given a  a path name,
     * return true if the path matches any of the patterns.
     * See more details in `man 3 fnmatch`.
     */
    bool match(const std::string &path, const int fnmatchFlags = 0,
               const bool isDirectory = false) const;

  private:
    /**
     * Remove `d_pathPrefix` and slashes from the beginning of `path`.
     * If  `d_pathPrefix` is empty or it is not the prefix of `path`,
     * the original `path` is returned.
     */
    std::string trimPrefix(const std::string &path) const;

    std::string d_pathPrefix;
    std::shared_ptr<std::vector<IgnorePattern>> d_ignorePatterns;
};

/**
 * Represents a directory that, optionally, has other directories inside.
 */
struct NestedDirectory {
  public:
    // Important to use a sorted map to keep subdirectories ordered by name
    typedef std::map<std::string, NestedDirectory> subdir_map;
    typedef std::map<std::string, NestedDirectory>::iterator subdir_map_it;
    std::unique_ptr<subdir_map> d_subdirs;
    // Important to use a sorted map to keep files ordered by name
    std::map<std::string, File> d_files;
    std::map<std::string, std::string> d_symlinks;
    NodeProperties d_nodeProperties;

    NestedDirectory() : d_subdirs(new subdir_map){};

    /**
     * Add the given File to this NestedDirectory at the given relative path,
     * which may include subdirectories.
     *
     *`tryAddFile()` returns `false` if the path already exists. `add()` throws
     * an exception if the path already exists.
     */
    bool tryAddFile(const File &file, const char *relativePath);
    void add(const File &file, const char *relativePath);

    /**
     * Add the given symlink to this NestedDirectory at the given relative
     * path, which may include subdirectories.
     *
     * `tryAddSymlink()` returns `false` if the path already exists.
     * `addSymlink()` throws an exception if the path already exists.
     */
    bool tryAddSymlink(const std::string &target, const char *relativePath);
    void addSymlink(const std::string &target, const char *relativePath);

    /**
     * Add the given Directory to this NestedDirectory at a given relative
     * path. If the directory has contents, the add method should be used
     * instead.
     *
     * `tryAddDirectory()` returns a pointer to the new or already existing
     * `NestedDirectory` or `nullptr` if the path already exists but is not a
     * directory. `addDirectory()` throws an exception if the path already
     * exists but is not a directory.
     */
    NestedDirectory *tryAddDirectory(const char *directory);
    void addDirectory(const char *directory);

    /**
     * Convert this NestedDirectory to a Directory message and return its
     * Digest.
     *
     * If a digestMap is passed, serialized Directory messages corresponding to
     * this directory and its subdirectories will be stored in it using their
     * Digest messages as the keys. (This is recursive -- nested subdirectories
     * will also be stored.
     */
    Digest to_digest(digest_string_map *digestMap = nullptr) const;

    /**
     * Add a key-value node property
     */
    void addNodeProperty(const std::string &name, const std::string &value);
    /**
     * Add a key-value node property pairs
     */
    void addNodeProperties(
        const std::map<std::string, std::string> &nodeProperties);

    /**
     * Convert this NestedDirectory to a Tree message.
     */
    Tree to_tree() const;

    void print(std::ostream &out, const std::string &dirName = "") const;

  private:
    bool getSubdirAndNameForAdd(const char *relativePath,
                                NestedDirectory **subdir, std::string *name);
};

/**
 * Create a NestedDirectory containing the contents of the given path and its
 * subdirectories.
 *
 * If a fileMap is passed, paths to all files referenced by the NestedDirectory
 * will be stored in it using their Digest messages as the keys.
 *
 * Unix_mode of a file can be masked by providing an updater function.
 */
NestedDirectory make_nesteddirectory(
    const char *path, digest_string_map *fileMap, const bool followSymlinks,
    const std::shared_ptr<IgnoreMatcher> &ignoreMatcher = nullptr,
    const UnixModeUpdater &unixModeUpdater = {}, ThreadPool *pool = nullptr);
NestedDirectory make_nesteddirectory(
    const char *path, digest_string_map *fileMap = nullptr,
    const std::vector<std::string> &capture_properties =
        std::vector<std::string>(),
    const bool followSymlinks = false,
    const std::shared_ptr<IgnoreMatcher> &ignoreMatcher = nullptr,
    const UnixModeUpdater &unixModeUpdater = {}, ThreadPool *pool = nullptr);
NestedDirectory make_nesteddirectory(
    const char *path, const FileDigestFunction &fileDigestFunc,
    digest_string_map *fileMap = nullptr,
    const std::vector<std::string> &capture_properties =
        std::vector<std::string>(),
    const bool followSymlinks = false,
    const std::shared_ptr<IgnoreMatcher> &ignoreMatcher = nullptr,
    const UnixModeUpdater &unixModeUpdater = {}, ThreadPool *pool = nullptr);
NestedDirectory make_nesteddirectory(
    int dirfd, digest_string_map *fileMap = nullptr,
    const std::vector<std::string> &capture_properties =
        std::vector<std::string>(),
    const bool followSymlinks = false,
    const std::shared_ptr<IgnoreMatcher> &ignoreMatcher = nullptr,
    const UnixModeUpdater &unixModeUpdater = {}, ThreadPool *pool = nullptr);
NestedDirectory make_nesteddirectory(
    int dirfd, const FileDigestFunction &fileDigestFunc,
    digest_string_map *fileMap = nullptr,
    const std::vector<std::string> &capture_properties =
        std::vector<std::string>(),
    const bool followSymlinks = false,
    const std::shared_ptr<IgnoreMatcher> &ignoreMatcher = nullptr,
    const UnixModeUpdater &unixModeUpdater = {}, ThreadPool *pool = nullptr);

std::ostream &operator<<(std::ostream &out, const NestedDirectory &obj);

} // namespace buildboxcommon

#endif
