#!/usr/bin/env bash
set -e

PGPASSWORD="insecure" psql -U bgd -d bgd -h database -p 5432 -f /app/data/revisions/all.sql
/app/env/bin/bgd $@
