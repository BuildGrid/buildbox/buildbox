#!/usr/bin/env bash
set -e

BUILDBOX_CASD_PORT=50011

test_grpc_health_check () {
    echo -e "\e[0;33m  Testing gRPC health check\e[0m\n"
    grpc_health_probe -addr=:$BUILDBOX_CASD_PORT -v -connect-timeout 10s
}


echo -e "\e[1;32mStarting buildbox-casd\e[0m\n"
buildbox-casd --cas-remote=http://buildgrid:50051 --bind=localhost:$BUILDBOX_CASD_PORT /tmp/casd &

echo -e "\e[1;39mRunning integration tests\e[0m"
test_grpc_health_check
