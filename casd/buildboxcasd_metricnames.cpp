// Copyright 2020 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <buildboxcasd_metricnames.h>

namespace buildboxcasd {
// Each metric is recorded in code with a statsd string. The statsd strings for
// each named metric are defined here. Only the constants are used in code, not
// the metric strings directly.

// CAS

// Elapsed wall time of Bytestream read operations
const std::string MetricNames::TIMER_NAME_CAS_BYTESTREAM_READ =
    "cas_bytestream_read";

// Sizes of blobs read with Bytestream.Read()
const std::string
    MetricNames::DISTRIBUTION_NAME_CAS_BYTESTREAM_READ_BLOB_SIZES =
        "cas_bytestream_read_blobs_sizes";

// Elapsed wall time of Bytestream write operations
const std::string MetricNames::TIMER_NAME_CAS_BYTESTREAM_WRITE =
    "cas_bytestream_write";

// Sizes of blobs read with Bytestream.Write()
const std::string
    MetricNames::DISTRIBUTION_NAME_CAS_BYTESTREAM_WRITE_BLOB_SIZES =
        "cas_bytestream_write_blobs_sizes";

// Elapsed wall time of a get capabilities request.
const std::string MetricNames::TIMER_NAME_CAS_GET_CAPABILITIES =
    "cas_get_capabilities";

// Elapsed wall time of remote FindMissingBlobs operations
const std::string MetricNames::TIMER_NAME_CAS_FIND_MISSING_BLOBS =
    "cas_find_missing_blobs";

// Elapsed wall time of remote BatchUpdateBlobs operations
const std::string MetricNames::TIMER_NAME_CAS_BATCH_UPDATE_BLOBS =
    "cas_batch_update_blobs";

// Elapsed wall time of remote BatchReadBlobs operations
const std::string MetricNames::TIMER_NAME_CAS_BATCH_READ_BLOBS =
    "cas_batch_read_blobs";

// Elapsed wall time of remote GetTree operations
const std::string MetricNames::TIMER_NAME_CAS_GET_TREE = "cas_get_tree";

// Running total number of blobs queried by `FindMissingBlobs()`
const std::string MetricNames::COUNTER_NUM_BLOBS_FIND_MISSING =
    "localcas_num_blobs_find_missing";

// Sizes of blobs queried in FindMissingBlobs() requests
const std::string MetricNames::DISTRIBUTION_NAME_CAS_FIND_MISSING_BLOBS_SIZES =
    "cas_find_missing_blobs_sizes";

// Running total number of blobs uploaded with `BatchUpdateBlobs()`
const std::string MetricNames::COUNTER_NUM_BLOBS_BATCH_UPDATE =
    "localcas_num_blobs_batch_update";

// Sizes of blobs queried in BatchUpdateBlobs() requests
const std::string MetricNames::DISTRIBUTION_NAME_CAS_BATCH_UPDATE_BLOB_SIZES =
    "cas_batch_update_blobs_sizes";

// Running total of blobs requested with `BatchReadBlobs()`
const std::string MetricNames::COUNTER_NUM_BLOBS_BATCH_READ =
    "localcas_num_blobs_batch_read";

// Sizes of blobs queried in BatchReadBlobs() requests
const std::string MetricNames::DISTRIBUTION_NAME_CAS_BATCH_READ_BLOBS_SIZES =
    "cas_batch_read_blobs_sizes";

// LOCAL CAS
// Running total of number of blobs read from remote storage
const std::string MetricNames::COUNTER_NUM_BLOBS_READ_FROM_REMOTE =
    "localcas_num_blobs_read_from_remote";

// Sizes of blobs requested in FetchMissingBlobs() requests
const std::string
    MetricNames::DISTRIBUTION_NAME_CAS_FETCH_MISSING_BLOBS_SIZES =
        "localcas_fetch_missing_blob_sizes";

// Elapsed wall time of local FindMissingBlobs operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_FETCH_MISSING_BLOB =
    "localcas_fetch_missing_blob";

// Elapsed wall time of local GetLocalServerDetails operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_GET_SERVER_DETAILS =
    "localcas_get_server_details";

// Sizes of blobs requested in UploadMissingBlobs() requests
const std::string
    MetricNames::DISTRIBUTION_NAME_CAS_UPLOAD_MISSING_BLOBS_SIZES =
        "localcas_upload_missing_blob_sizes";

// Elapsed wall time of local UploadMissingBlobs operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_UPLOAD_MISSING_BLOB =
    "localcas_upload_missing_blob";

// Elapsed wall time of local FetchTree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_FETCH_TREE =
    "localcas_fetch_tree";

// Elapsed wall time of local UploadTree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_UPLOAD_TREE =
    "localcas_upload_tree";

// Elapsed wall time of local StageTree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_STAGE_TREE_TOTAL =
    "localcas_stage_tree_total";

// Elapsed wall time of just the prepare tree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_STAGE_TREE_PREPARE =
    "localcas_stage_tree_prepare";

// Elapsed wall time of just the staging operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_STAGE_TREE_STAGE =
    "localcas_stage_tree_stage";

// Elapsed wall time of local CaptureTree operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_CAPTURE_TREE =
    "localcas_capture_tree";

// Elapsed wall time of local CaptureFiles operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_CAPTURE_FILES =
    "localcas_capture_files";

// Elapsed wall time of local GetInstanceNameForRemote operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_GET_INSTANCE_FROM_REMOTE =
    "localcas_get_instance_from_remote";

// Elapsed wall time of local GetLocalDiskUsage operations
const std::string MetricNames::TIMER_NAME_LOCAL_CAS_GET_LOCAL_DISK_USAGE =
    "localcas_get_local_disk_usage";

// Running total of number of blobs written to remote storage
const std::string MetricNames::COUNTER_NUM_BLOBS_WRITTEN_TO_REMOTE =
    "localcas_num_blobs_written_to_remote";

// Number of bytes read from a remote CAS when configured as a proxy
const std::string MetricNames::COUNTER_NAME_REMOTE_BYTES_READ =
    "localcas_remote_bytes_read";

// Number of bytes written to a remote CAS when configured as a proxy
const std::string MetricNames::COUNTER_NAME_REMOTE_BYTES_WRITE =
    "localcas_remote_bytes_write";

// Number of blobs updated via capture requests
const std::string MetricNames::COUNTER_NUM_BLOBS_CAPTURED =
    "localcas_num_blobs_captured";

// Number of `FetchTree()` requests issued:
const std::string MetricNames::COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_REQUESTS =
    "localcas_num_fetch_tree_requests";

// Number of `FetchTree()` requests serviced from cache (without scanning
// local storage):
const std::string
    MetricNames::COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_CACHE_HITS =
        "localcas_num_fetch_tree_cache_hits";

// Cache hits in per-directory tree cache used by `FetchTree()` and
// `StageTree()`
const std::string MetricNames::COUNTER_NAME_LOCAL_CAS_TREE_CACHE_HITS =
    "localcas_num_tree_cache_hits";

// Cache misses in per-directory tree cache used by `FetchTree()` and
// `StageTree()`
const std::string MetricNames::COUNTER_NAME_LOCAL_CAS_TREE_CACHE_MISSES =
    "localcas_num_tree_cache_misses";

// Percentage of per-directory tree cache hits in a given gRPC request
const std::string
    MetricNames::DISTRIBUTION_NAME_LOCAL_CAS_TREE_CACHE_HIT_PERCENTAGE =
        "localcas_tree_cache_hit_percentage";

// ActionCache
const std::string MetricNames::TIMER_NAME_AC_GET_ACTION_RESULT =
    "ac_get_action_result";
const std::string MetricNames::TIMER_NAME_AC_UPDATE_ACTION_RESULT =
    "ac_update_action_result";
const std::string MetricNames::COUNTER_NAME_AC_GET_ACTION_RESULT_HITS =
    "ac_num_get_action_result_hits";
const std::string MetricNames::COUNTER_NAME_AC_GET_ACTION_RESULT_MISSES =
    "ac_num_get_action_result_misses";
const std::string
    MetricNames::COUNTER_NAME_AC_GET_ACTION_RESULT_OUTPUT_MISSES =
        "ac_num_get_action_result_output_misses";

} // namespace buildboxcasd
