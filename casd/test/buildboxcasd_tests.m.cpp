/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>
#include <signal.h>

#include <buildboxcommon_logging.h>

int main(int argc, char **argv)
{
    ::testing::InitGoogleTest(&argc, argv);

    // Reduce noise in unit test outputs.
    // gitlab currently truncating logs.
    BUILDBOX_LOG_SET_LEVEL(buildboxcommon::LogLevel::WARNING);

    // Ignore SIGPIPE in case of using sockets + grpc without MSG_NOSIGNAL
    // support configured
    struct sigaction new_sig;
    new_sig.sa_handler = SIG_IGN;
    new_sig.sa_flags = 0;
    sigfillset(&new_sig.sa_mask);
    sigaction(SIGPIPE, &new_sig, NULL);

    return RUN_ALL_TESTS();
}
