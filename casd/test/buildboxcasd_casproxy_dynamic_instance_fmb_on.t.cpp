#include <buildboxcasd_casproxy.h>

INSTANTIATE_TEST_SUITE_P(
    DynamicInstanceFmbOn, CasProxyFixture,
    testing::Combine(testing::Values(DYNAMIC_INSTANCE),
                     testing::Values(FINDMISSINGBLOBS_CACHE_ON),
                     testing::ValuesIn(TEST_INSTANCE_NAMES),
                     testing::Values(NON_THREADED, THREADED)));

GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(
    CasProxyUnReachableRemoteFixture);
GTEST_ALLOW_UNINSTANTIATED_PARAMETERIZED_TEST(
    CasProxyWithFindMissingBlobsCacheFixture);
