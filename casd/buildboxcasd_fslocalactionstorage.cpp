/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_fslocalactionstorage.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_fslocalcas.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_systemutils.h>
#include <buildboxcommon_temporaryfile.h>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <stdio.h>

using namespace buildboxcasd;
using namespace buildboxcommon;

FsLocalActionStorage::FsLocalActionStorage(const std::string &path)
{
    std::string actioncache_path = path;
    if (path[path.length() - 1] != '/') {
        actioncache_path += "/";
    }
    actioncache_path += "actioncache/";

    buildboxcommon::FileUtils::createDirectory(actioncache_path.c_str());

    d_pwd = actioncache_path;
    BUILDBOX_LOG_DEBUG("ActionCache storage using path " + actioncache_path);
}

static void renameTemp(const std::string &final_path,
                       const std::string &temp_path)
{
    const int result = rename(temp_path.c_str(), final_path.c_str());
    if (result != 0) {
        BUILDBOXCOMMON_THROW_SYSTEM_EXCEPTION(
            std::system_error, errno, std::system_category,
            "Could not copy temporary file " << temp_path << " to "
                                             << final_path);
    }
}

void FsLocalActionStorage::storeAction(const Digest &action_digest,
                                       const ActionResult &result)
{
    const std::string final_filename = d_pwd + action_digest.hash();

    buildboxcommon::TemporaryFile temp_file(d_pwd.c_str(),
                                            "actioncache-tmpfile");
    const std::string temp_filename(temp_file.name());

    std::ofstream file(temp_filename, std::fstream::binary);
    result.SerializeToOstream(&file);

    if (!file.good()) {
        const std::string error_message = "Failed writing to temporary file " +
                                          temp_filename + ": " +
                                          strerror(errno);
        BUILDBOX_LOG_ERROR(error_message);

        if (errno == ENOSPC) {
            BUILDBOXCOMMON_THROW_EXCEPTION(OutOfSpaceException, error_message);
        }
        else {
            BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error, error_message);
        }
    }

    renameTemp(final_filename, temp_filename);
}

bool FsLocalActionStorage::readAction(const Digest &action_digest,
                                      ActionResult *result)
{
    std::ifstream action_file(d_pwd + action_digest.hash(),
                              std::fstream::binary);

    if (action_file.fail()) {
        return false;
    }

    if (!result->ParseFromIstream(&action_file)) {
        BUILDBOX_LOG_DEBUG("File " + d_pwd + action_digest.hash() +
                           " was empty");
        return false;
    }

    return true;
}
