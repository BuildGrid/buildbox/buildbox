// Copyright 2020 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef BUILDBOXCASD_METRICNAMES_H
#define BUILDBOXCASD_METRICNAMES_H

#include <buildboxcommon_metricnames.h>

#include <string>

namespace buildboxcasd {

class MetricNames : public buildboxcommon::CommonMetricNames {
  private:
    MetricNames() = delete;

  public:
    // Structure to contain the static statsd strings under which metrics are
    // published. Refer to buildboxcasd_metricnames.cpp to determine the
    // definition of the metric and the associated statsd name.

    /*
     * CAS
     */
    // Bytestream.Read()
    static const std::string DISTRIBUTION_NAME_CAS_BYTESTREAM_READ_BLOB_SIZES;
    static const std::string TIMER_NAME_CAS_BYTESTREAM_READ;

    // Bytestream.Write()
    static const std::string DISTRIBUTION_NAME_CAS_BYTESTREAM_WRITE_BLOB_SIZES;
    static const std::string TIMER_NAME_CAS_BYTESTREAM_WRITE;

    // GetCapabilities()
    static const std::string TIMER_NAME_CAS_GET_CAPABILITIES;

    // FindMissingBlobs()
    static const std::string COUNTER_NUM_BLOBS_FIND_MISSING;
    static const std::string DISTRIBUTION_NAME_CAS_FIND_MISSING_BLOBS_SIZES;
    static const std::string TIMER_NAME_CAS_FIND_MISSING_BLOBS;

    // BatchUpdateBlobs()
    static const std::string COUNTER_NUM_BLOBS_BATCH_UPDATE;
    static const std::string DISTRIBUTION_NAME_CAS_BATCH_UPDATE_BLOB_SIZES;
    static const std::string TIMER_NAME_CAS_BATCH_UPDATE_BLOBS;

    // BatchReadBlobs()
    static const std::string COUNTER_NUM_BLOBS_BATCH_READ;
    static const std::string DISTRIBUTION_NAME_CAS_BATCH_READ_BLOBS_SIZES;
    static const std::string TIMER_NAME_CAS_BATCH_READ_BLOBS;

    // GetTree()
    static const std::string TIMER_NAME_CAS_GET_TREE;

    /*
     * LOCAL CAS
     */
    static const std::string COUNTER_NUM_BLOBS_READ_FROM_REMOTE;
    static const std::string COUNTER_NUM_BLOBS_WRITTEN_TO_REMOTE;
    static const std::string COUNTER_NAME_REMOTE_BYTES_READ;
    static const std::string COUNTER_NAME_REMOTE_BYTES_WRITE;

    // GetLocalServerDetails()
    static const std::string TIMER_NAME_LOCAL_CAS_GET_SERVER_DETAILS;

    // FetchMissingBlobs()
    static const std::string DISTRIBUTION_NAME_CAS_FETCH_MISSING_BLOBS_SIZES;
    static const std::string TIMER_NAME_LOCAL_CAS_FETCH_MISSING_BLOB;

    // UploadMissingBlobs()
    static const std::string DISTRIBUTION_NAME_CAS_UPLOAD_MISSING_BLOBS_SIZES;
    static const std::string TIMER_NAME_LOCAL_CAS_UPLOAD_MISSING_BLOB;

    // FetchTree()
    static const std::string COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_REQUESTS;
    static const std::string COUNTER_NAME_LOCAL_CAS_NUM_FETCH_TREE_CACHE_HITS;

    static const std::string TIMER_NAME_LOCAL_CAS_FETCH_TREE;
    static const std::string TIMER_NAME_LOCAL_CAS_UPLOAD_TREE;

    // Stage
    static const std::string COUNTER_NUM_HARDLINK_STAGER_STAGED_FILES;
    static const std::string COUNTER_NUM_HARDLINK_STAGER_STAGED_DIRECTORIES;
    static const std::string COUNTER_NUM_HARDLINK_STAGER_STAGED_SYMLINKS;

    static const std::string TIMER_NAME_LOCAL_CAS_STAGE_TREE_TOTAL;
    static const std::string TIMER_NAME_LOCAL_CAS_STAGE_TREE_PREPARE;
    static const std::string TIMER_NAME_LOCAL_CAS_STAGE_TREE_STAGE;

    // Capture
    static const std::string COUNTER_NUM_BLOBS_CAPTURED;

    static const std::string TIMER_NAME_LOCAL_CAS_CAPTURE_TREE;
    static const std::string TIMER_NAME_LOCAL_CAS_CAPTURE_FILES;
    static const std::string TIMER_NAME_LOCAL_CAS_GET_INSTANCE_FROM_REMOTE;
    static const std::string TIMER_NAME_LOCAL_CAS_GET_LOCAL_DISK_USAGE;

    // Per-directory tree cache used by FetchTree() and StageTree()
    static const std::string COUNTER_NAME_LOCAL_CAS_TREE_CACHE_HITS;
    static const std::string COUNTER_NAME_LOCAL_CAS_TREE_CACHE_MISSES;
    static const std::string
        DISTRIBUTION_NAME_LOCAL_CAS_TREE_CACHE_HIT_PERCENTAGE;

    /*
     * ActionCache
     */
    static const std::string TIMER_NAME_AC_GET_ACTION_RESULT;
    static const std::string TIMER_NAME_AC_UPDATE_ACTION_RESULT;
    static const std::string COUNTER_NAME_AC_GET_ACTION_RESULT_HITS;
    static const std::string COUNTER_NAME_AC_GET_ACTION_RESULT_MISSES;
    static const std::string COUNTER_NAME_AC_GET_ACTION_RESULT_OUTPUT_MISSES;
};

} // namespace buildboxcasd

#endif
