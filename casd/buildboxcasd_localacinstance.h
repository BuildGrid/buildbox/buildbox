/*
 * Copyright 2021 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_LOCALACINSTANCE_H
#define INCLUDED_BUILDBOXCASD_LOCALACINSTANCE_H

#include <buildboxcasd_acinstance.h>
#include <buildboxcasd_actionstorage.h>
#include <buildboxcasd_casinstance.h>
#include <buildboxcommon_protos.h>
#include <grpc/grpc.h>

using namespace build::bazel::remote::execution::v2;
using namespace build::buildgrid;

namespace buildboxcasd {

class LocalAcInstance : public AcInstance {
    /* Implements the reAPI ActionCache protocol. Runs locally on one machine.
     * Currently supports using the machine's filesystem to hold the cache. all
     * errors will be reported in the `status` field of the `ExecuteResponse`
     */
  public:
    LocalAcInstance(std::shared_ptr<ActionStorage> storage,
                    std::shared_ptr<CasInstance> cas)
        : AcInstance(storage, cas){};
    ~LocalAcInstance() override = default;

    // Implements virtual functions from the ACInstance interface
    grpc::Status GetActionResult(const GetActionResultRequest &request,
                                 ActionResult *result) override;
    grpc::Status UpdateActionResult(const UpdateActionResultRequest &request,
                                    ActionResult *result) override;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_LOCALACINSTANCE_H
