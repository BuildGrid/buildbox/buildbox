/*
 * Copyright 2023 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_executioninstance.h>
#include <buildboxcommon_protos.h>

using namespace build::bazel::remote::execution::v2;
using namespace google::longrunning;

using grpc::Status;

namespace buildboxcasd {

ExecutionInstance::ExecutionInstance(const std::string &instance_name)
    : d_instance_name(instance_name)
{
}
ExecutionInstance::~ExecutionInstance() {}

grpc::Status ExecutionInstance::Execute(ServerContext *,
                                        const ExecuteRequest &,
                                        ServerWriterInterface<Operation> *)
{
    return grpc::Status(grpc::StatusCode::UNIMPLEMENTED, __func__);
}

grpc::Status
ExecutionInstance::WaitExecution(ServerContext *, const WaitExecutionRequest &,
                                 ServerWriterInterface<Operation> *)
{
    return grpc::Status(grpc::StatusCode::UNIMPLEMENTED, __func__);
}

grpc::Status ExecutionInstance::GetOperation(const GetOperationRequest &,
                                             Operation *)
{
    return grpc::Status(grpc::StatusCode::UNIMPLEMENTED, __func__);
}

grpc::Status ExecutionInstance::ListOperations(const ListOperationsRequest &,
                                               ListOperationsResponse *)
{
    return grpc::Status(grpc::StatusCode::UNIMPLEMENTED, __func__);
}

grpc::Status ExecutionInstance::CancelOperation(const CancelOperationRequest &,
                                                google::protobuf::Empty *)
{
    return grpc::Status(grpc::StatusCode::UNIMPLEMENTED, __func__);
}

void ExecutionInstance::stop() {}

} // namespace buildboxcasd