#include <gtest/gtest.h>

#include <buildboxworker_logstreamdebugutils.h>

TEST(LogStreamDebugUtilsTest, CommandWithoutPlaceholder)
{
    std::string commandString("./debug-script.sh --flag");
    const std::string resourceName1("logstream/stdout");
    const std::string resourceName2("logstream/stderr");

    buildboxworker::LogStreamDebugUtils::populateResourceNameArguments(
        resourceName1, resourceName2, &commandString);

    ASSERT_EQ(commandString, "./debug-script.sh --flag");
}

TEST(LogStreamDebugUtilsTest, CommandWithOnePlaceholder)
{
    std::string commandString =
        "/usr/bin/command --argument1=\"{stdout}\" argument2";
    const std::string stdoutResourceName("logstream/resource/stdout");
    const std::string stderrResourceName("");

    buildboxworker::LogStreamDebugUtils::populateResourceNameArguments(
        stdoutResourceName, stderrResourceName, &commandString);

    ASSERT_EQ(commandString,
              "/usr/bin/command --argument1=\"logstream/resource/stdout\" "
              "argument2");
}

TEST(LogStreamDebugUtilsTest, CommandWithTwoPlaceholders)
{
    std::string commandString =
        "/usr/bin/command --stdout-stream=\"{stdout}\" "
        "--stderr-stream=\"{stderr}\" --print=true";
    const std::string stdoutResourceName("logstream/resource/stdout");
    const std::string stderrResourceName("logstream/resource/stderr");

    buildboxworker::LogStreamDebugUtils::populateResourceNameArguments(
        stdoutResourceName, stderrResourceName, &commandString);

    ASSERT_EQ(commandString,
              "/usr/bin/command --stdout-stream=\"logstream/resource/stdout\" "
              "--stderr-stream=\"logstream/resource/stderr\" --print=true");
}

TEST(LogStreamDebugUtilsTest, FindAndReplace)
{
    std::string s = "abc**def**ghi*";

    buildboxworker::LogStreamDebugUtils::findAndReplace("**", "-", &s);

    ASSERT_EQ(s, "abc-def-ghi*");
}

TEST(LogStreamDebugUtilsTest, FindAndReplaceNoMatch)
{
    std::string s = "A B C";

    buildboxworker::LogStreamDebugUtils::findAndReplace(".", "-", &s);

    ASSERT_EQ(s, "A B C");
}
