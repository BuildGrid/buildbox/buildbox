// Copyright 2019 Bloomberg Finance L.P
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include <buildboxcommon_casclient.h>
#include <buildboxcommon_commandline.h>
#include <buildboxcommon_digestgenerator.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_merklize.h>

#include <cstddef>
#include <cstdlib>
#include <cstring>
#include <filesystem>
#include <fstream>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>
#include <grpcpp/support/status.h>
#include <iostream>
#include <memory>
#include <processargs.h>
#include <sstream>
#include <stdexcept>
#include <sys/stat.h>
#include <ThreadPool.h>
#include <utility>

void uploadResources(
    const buildboxcommon::digest_string_map &blobs,
    const buildboxcommon::digest_string_map &digest_to_filepaths,
    const std::unique_ptr<buildboxcommon::CASClient> &casClient,
    ThreadPool *uploadThreadPool)
{
    std::vector<buildboxcommon::Digest> digestsToUpload;
    for (const auto &i : blobs) {
        digestsToUpload.push_back(i.first);
    }
    for (const auto &i : digest_to_filepaths) {
        digestsToUpload.push_back(i.first);
    }

    const auto missingDigests = casClient->findMissingBlobs(digestsToUpload);

    std::vector<buildboxcommon::CASClient::UploadRequest> upload_requests;
    upload_requests.reserve(missingDigests.size());
    for (const auto &digest : missingDigests) {
        // Finding the data in one of the source maps:
        if (blobs.count(digest)) {
            upload_requests.emplace_back(
                buildboxcommon::CASClient::UploadRequest(digest,
                                                         blobs.at(digest)));
        }
        else if (digest_to_filepaths.count(digest)) {
            upload_requests.emplace_back(
                buildboxcommon::CASClient::UploadRequest::from_path(
                    digest, digest_to_filepaths.at(digest)));
        }
        else {
            throw std::runtime_error(
                "FindMissingBlobs returned non-existent digest");
        }
    }

    const auto failedToUpload =
        casClient->uploadBlobs(upload_requests, uploadThreadPool);
    if (!failedToUpload.empty()) {
        const auto &firstError = failedToUpload.front().status;
        BUILDBOXCOMMON_THROW_EXCEPTION(
            std::runtime_error,
            "Failed to upload "
                << failedToUpload.size()
                << " blobs. First error: code=" << firstError.error_code()
                << " message=" << firstError.error_message());
    }
}

void uploadDirectory(
    const std::string &path, const buildboxcommon::Digest &digest,
    const buildboxcommon::digest_string_map &digestsToBlobs,
    const buildboxcommon::digest_string_map &directoryDigestToFilepaths,
    const std::unique_ptr<buildboxcommon::CASClient> &casClient,
    ThreadPool *uploadThreadPool)
{
    assert(casClient != nullptr);

    try {
        BUILDBOX_LOG_DEBUG("Starting to upload merkle tree...");
        uploadResources(digestsToBlobs, directoryDigestToFilepaths, casClient,
                        uploadThreadPool);
        BUILDBOX_LOG_INFO("Uploaded \"" << path << "\": " << digest.hash()
                                        << "/" << digest.size_bytes());
    }
    catch (const std::runtime_error &e) {
        BUILDBOX_LOG_ERROR("Uploading " << path
                                        << " failed with error: " << e.what());
        exit(1);
    }
}

std::pair<buildboxcommon::Digest, // rootDigest
          buildboxcommon::Digest  // treeDigest
          >
processDirectory(
    const std::string &path, const bool followSymlinks,
    const std::unique_ptr<buildboxcommon::CASClient> &casClient,
    const std::shared_ptr<std::vector<buildboxcommon::IgnorePattern>>
        &ignorePatterns,
    const std::vector<std::string> &captureProperties,
    const mode_t unixModeMask,
    const std::map<std::string, std::string> &nodeProperties,
    ThreadPool *uploadThreadPool, ThreadPool *digestThreadPool,
    const buildboxcommon::Command_OutputDirectoryFormat dirFormat,
    const casupload::LocalCasOption &localCasOption)
{
    // Note: LocalCAS::CaptureTree doesn't support following symlink yet
    if (localCasOption.d_useLocalCas && !followSymlinks &&
        casClient != nullptr) {
        const auto captureResponse =
            casClient
                ->captureTree({std::filesystem::absolute(path)},
                              captureProperties,
                              localCasOption.d_bypassLocalCache, unixModeMask,
                              nullptr, dirFormat)
                .responses()
                .at(0);
        if (captureResponse.status().code() != grpc::StatusCode::OK) {
            BUILDBOXCOMMON_THROW_EXCEPTION(
                std::runtime_error,
                "Failed to capture directory \""
                    << path << "\" code=" << captureResponse.status().code()
                    << " message=" << captureResponse.status().message());
        }
        const auto rootDigest = captureResponse.root_directory_digest();
        const auto treeDigest = captureResponse.tree_digest();

        std::ostringstream ss;
        ss << "Captured \"" << path << "\":";
        if (rootDigest.ByteSizeLong() != 0) {
            ss << " rootDirectoryDigest=" << rootDigest.hash() << "/"
               << rootDigest.ByteSizeLong();
        }
        if (treeDigest.ByteSizeLong() != 0) {
            ss << " treeDigest=" << treeDigest.hash() << "/"
               << treeDigest.ByteSizeLong();
        }
        BUILDBOX_LOG_INFO(ss.str())

        return std::make_pair<const buildboxcommon::Digest,
                              const buildboxcommon::Digest>(
            std::move(rootDigest), std::move(treeDigest));
    }

    // contains Directories and optionally a Tree
    buildboxcommon::digest_string_map digestsToBlobs;
    buildboxcommon::digest_string_map directoryDigestToFilepaths;

    const auto ignoreMatcher =
        ignorePatterns == nullptr
            ? nullptr
            : std::make_shared<buildboxcommon::IgnoreMatcher>(path,
                                                              ignorePatterns);

    auto singleNestedDirectory = buildboxcommon::make_nesteddirectory(
        path.c_str(), &directoryDigestToFilepaths, captureProperties,
        followSymlinks, ignoreMatcher,
        buildboxcommon::unixModeMaskUpdater(unixModeMask), digestThreadPool);
    singleNestedDirectory.addNodeProperties(nodeProperties);

    const auto rootDigest = singleNestedDirectory.to_digest(&digestsToBlobs);
    buildboxcommon::Digest treeDigest;
    if (dirFormat != buildboxcommon::Command_OutputDirectoryFormat::
                         Command_OutputDirectoryFormat_DIRECTORY_ONLY) {
        const auto tree = singleNestedDirectory.to_tree().SerializeAsString();
        treeDigest = buildboxcommon::DigestGenerator::hash(tree);
        BUILDBOX_LOG_DEBUG("Computed tree digest for \""
                           << path << "\": " << treeDigest.hash() << "/"
                           << treeDigest.size_bytes());
        digestsToBlobs.emplace(treeDigest, std::move(tree));
    }

    BUILDBOX_LOG_DEBUG("Finished building nested directory from \""
                       << path << "\": " << rootDigest.hash() << "/"
                       << rootDigest.size_bytes());
    BUILDBOX_LOG_DEBUG(singleNestedDirectory);

    if (casClient == nullptr) {
        BUILDBOX_LOG_INFO("Computed directory digest for \""
                          << path << "\": " << rootDigest.hash() << "/"
                          << rootDigest.size_bytes());
    }
    else {
        uploadDirectory(path, rootDigest, digestsToBlobs,
                        directoryDigestToFilepaths, casClient,
                        uploadThreadPool);
    }

    return std::make_pair<const buildboxcommon::Digest,
                          buildboxcommon::Digest>(std::move(rootDigest),
                                                  std::move(treeDigest));
}

std::pair<buildboxcommon::Digest, // rootDigest
          buildboxcommon::Digest  // treeDigest
          >
processPath(const std::string &path, const bool followSymlinks,
            buildboxcommon::NestedDirectory *nestedDirectory,
            buildboxcommon::digest_string_map *digestToFilePaths,
            const std::unique_ptr<buildboxcommon::CASClient> &casClient,
            const std::shared_ptr<std::vector<buildboxcommon::IgnorePattern>>
                &ignorePatterns,
            const std::vector<std::string> &captureProperties,
            const mode_t unixModeMask,
            const std::map<std::string, std::string> &nodeProperties,
            ThreadPool *uploadThreadPool, ThreadPool *digestThreadPool,
            const buildboxcommon::Command_OutputDirectoryFormat dirFormat,
            const casupload::LocalCasOption &localCasOption)
{
    BUILDBOX_LOG_DEBUG("Starting to process \""
                       << path << "\", followSymlinks = " << std::boolalpha
                       << followSymlinks << std::noboolalpha);

    const std::filesystem::file_status stat =
        followSymlinks ? std::filesystem::status(path)
                       : std::filesystem::symlink_status(path);

    if (stat.type() == std::filesystem::file_type::directory) {
        return processDirectory(path, followSymlinks, casClient,
                                ignorePatterns, captureProperties,
                                unixModeMask, nodeProperties, uploadThreadPool,
                                digestThreadPool, dirFormat, localCasOption);
    }
    else if (stat.type() == std::filesystem::file_type::symlink) {
        const auto target = std::filesystem::read_symlink(path);
        nestedDirectory->addSymlink(target, path.c_str());
    }
    else if (stat.type() == std::filesystem::file_type::regular) {
        const buildboxcommon::File file(
            path.c_str(), captureProperties,
            buildboxcommon::unixModeMaskUpdater(unixModeMask), nodeProperties);
        nestedDirectory->add(file, path.c_str());
        digestToFilePaths->emplace(file.d_digest, path);
    }
    else {
        BUILDBOX_LOG_DEBUG("Encountered unsupported file \""
                           << path << "\", skipping...");
    }
    return std::make_pair(buildboxcommon::Digest(), buildboxcommon::Digest());
}

void writeDigestToFile(const buildboxcommon::Digest &digest,
                       const std::string &path)
{
    std::ofstream digest_file;
    digest_file.open(path);
    digest_file << digest.hash() << "/" << digest.size_bytes();
    digest_file.close();
}

int main(int argc, char *argv[])
{
    std::vector<std::string> cliArgs(argv, argv + argc);
    buildboxcommon::logging::Logger::getLoggerInstance().initialize(
        cliArgs[0].c_str());

    auto args = casupload::processArgs(argc, argv);
    if (args.d_processed) {
        return 0;
    }
    if (!args.d_valid) {
        return 2;
    }

    BUILDBOX_LOG_SET_LEVEL(args.d_logLevel);

    buildboxcommon::DigestGenerator::init(args.d_digestFunctionValue);

    if (args.d_paths.size() > 1 && (!args.d_outputRootDigestFile.empty() ||
                                    !args.d_outputTreeDigestFile.empty())) {
        // Making sure that we'll produce a single directory digest:
        const auto directoryPath = std::find_if(
            args.d_paths.cbegin(), args.d_paths.cend(),
            [args](const std::string &path) {
                return args.d_followSymlinks
                           ? buildboxcommon::FileUtils::isDirectory(
                                 path.c_str())
                           : buildboxcommon::FileUtils::isDirectoryNoFollow(
                                 path.c_str());
            });

        if (directoryPath != args.d_paths.cend()) {
            BUILDBOX_LOG_ERROR(
                "`--output-digest-file` and `--output-tree-digest-file` can "
                "be used only when uploading a single directory structure");
            return 2;
        }
    }

    std::shared_ptr<std::vector<buildboxcommon::IgnorePattern>> ignorePatterns;
    if (args.d_ignoreFile != "") {
        std::ifstream ifs(args.d_ignoreFile);
        if (!ifs.good()) {
            BUILDBOX_LOG_ERROR(
                "Unable to read from file specified in --ignore-file");
            return 2;
        }
        ignorePatterns =
            buildboxcommon::IgnoreMatcher::parseIgnorePatterns(ifs);
    }

    // CAS client object (we don't initialize it if `dryRunMode` is set):
    std::unique_ptr<buildboxcommon::CASClient> casClient;

    try {
        if (!args.d_dryRunMode) {
            auto grpcClient = std::make_shared<buildboxcommon::GrpcClient>();
            grpcClient->init(args.d_casConnectionOptions);
            casClient =
                std::make_unique<buildboxcommon::CASClient>(grpcClient);
            casClient->init();
        }
    }
    catch (std::runtime_error &e) {
        std::cerr << "Failed to connect to CAS server: " << e.what()
                  << std::endl;
        return 1;
    }

    std::vector<std::string> captureProperties;
    if (args.d_captureMtime) {
        captureProperties.emplace_back("mtime");
    }
    if (args.d_captureUnixMode) {
        captureProperties.emplace_back("unix_mode");
    }

    buildboxcommon::NestedDirectory nestedDirectory;
    buildboxcommon::digest_string_map digestToFilePaths;

    const auto &localCasOption = args.d_localCasOption;

    // Thread pools
    // Disabled if local CAS is used
    std::unique_ptr<ThreadPool> uploadThreadPool = nullptr,
                                digestThreadPool = nullptr;
    if (!localCasOption.d_useLocalCas && args.d_numUploadThreads > 0) {
        uploadThreadPool =
            std::make_unique<ThreadPool>(args.d_numUploadThreads);
    }
    if (!localCasOption.d_useLocalCas && args.d_numDigestThreads > 0) {
        digestThreadPool =
            std::make_unique<ThreadPool>(args.d_numDigestThreads);
    }

    // Upload directories individually, and aggregate files to upload as single
    // merkle tree
    std::vector<buildboxcommon::Digest> directory_digests;
    for (const auto &path : args.d_paths) {
        auto [dirRootDigest, treeDigest] = processPath(
            path, args.d_followSymlinks, &nestedDirectory, &digestToFilePaths,
            casClient, ignorePatterns, captureProperties, args.d_fileUmask,
            args.d_nodeProperties, uploadThreadPool.get(),
            digestThreadPool.get(), args.d_directoryFormat, localCasOption);

        // Upload single files
        if (!digestToFilePaths.empty()) {
            BUILDBOX_LOG_DEBUG("Building nested directory structure...");
            buildboxcommon::digest_string_map blobs;
            const auto directoryDigest = nestedDirectory.to_digest(&blobs);

            BUILDBOX_LOG_INFO("Computed directory digest: "
                              << directoryDigest.hash() << "/"
                              << directoryDigest.size_bytes());

            if (!args.d_dryRunMode) {
                try {
                    uploadResources(blobs, digestToFilePaths, casClient,
                                    uploadThreadPool.get());
                    BUILDBOX_LOG_DEBUG("Files uploaded successfully");
                }
                catch (const std::runtime_error &e) {
                    BUILDBOX_LOG_ERROR(
                        "Uploading files failed with error: " << e.what());
                    return 1;
                }
            }
        }

        if (args.d_directoryFormat == buildboxcommon::Command::TREE_ONLY ||
            args.d_directoryFormat ==
                buildboxcommon::Command::TREE_AND_DIRECTORY) {

            // Invoked for a single directory.
            // Write the tree digest to the outputTreeDigestFile
            if (!args.d_outputTreeDigestFile.empty() &&
                args.d_paths.size() == 1) {
                BUILDBOX_LOG_DEBUG("Writing tree digest ["
                                   << treeDigest << "] to '"
                                   << args.d_outputTreeDigestFile << "'");
                writeDigestToFile(treeDigest, args.d_outputTreeDigestFile);
            }
        }

        if (args.d_directoryFormat ==
                buildboxcommon::Command::DIRECTORY_ONLY ||
            args.d_directoryFormat ==
                buildboxcommon::Command::TREE_AND_DIRECTORY) {

            // Invoked for a single directory.
            // Write the root digest to the outputRootDigestFile
            if (!args.d_outputRootDigestFile.empty() &&
                args.d_paths.size() == 1) {
                BUILDBOX_LOG_DEBUG("Writing root digest ["
                                   << dirRootDigest << "] to '"
                                   << args.d_outputRootDigestFile << "'");
                writeDigestToFile(dirRootDigest, args.d_outputRootDigestFile);
            }
        }
    }

    return 0;
}
