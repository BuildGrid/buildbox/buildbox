#casupload

`casupload` facilitates uploading blobs, action outputs and directory trees
to a previously configured Content Addressable Store and Action Cache (such
as BuildGrid, Buildbarn or Buildfarm).

## Usage

```
Usage: ./casupload
   --help                      Display usage and exit.
   --version                   Print version information and exit. [optional]
   --remote                    URL for the CAS service [optional]
   --instance                  Name of the CAS instance [optional]
   --server-cert               Server TLS certificate for CAS (PEM-encoded) [optional]
   --client-key                Client private TLS key for CAS (PEM-encoded) [optional]
   --client-cert               Client TLS certificate for CAS (PEM-encoded) [optional]
   --access-token              Authentication token for CAS (JWT, OAuth token etc), will be included as an HTTP  Authorization bearer token [optional]
   --token-reload-interval     Access token refresh timeout for CAS service [optional]
   --googleapi-auth            Use GoogleAPIAuth for CAS service [optional]
   --retry-limit               Retry limit for gRPC errors for CAS service [optional]
   --retry-delay               Retry delay for gRPC errors for CAS service [optional]
   --retry-on-code             gRPC status code(s) as string(s) to retry on for CAS service e.g., 'UNKNOWN', 'INTERNAL' [optional]
   --request-timeout           Timeout for gRPC requests for CAS service (set to 0 to disable timeout) [optional]
   --min-throughput            Minimum throughput for gRPC requests for CAS service, bytes per seconds. The value may be suffixed with K, M, G or T. [optional]
   --keepalive-time            gRPC keepalive pings period for CAS service (set to 0 to disable keepalive pings) [optional]
   --load-balancing-policy     gRPC load balancing policy for CAS service (valid options are 'round_robin' and 'grpclb') [optional]
   --follow-symlinks           Follow symlinks in the inputs [optional]
   --capture-mtime             Capture mtime of files to be uploaded [optional]
   --capture-unix-mode         Capture Unix mode of files to be uploaded [optional]
   --file-umask                Bit mask in the octal format to turn off bits in captured unix modes of files,
                                  e.g. "--file-umask=0222" makes all files non-writable [optional]
   --node-property             Node property attached to the uploaded path,
                                  e.g. "--node-property foo=bar" [optional]
   --dry-run                   Calculate and print digests, do not upload anything [optional]
   --digest-function           Set a custom digest function. Default: SHA256
                                  Supported functions: SHA384, SHA512, SHA256, SHA1, MD5 [optional]
   --directory-format          Set the format to upload directories in. 
                                  "--directory-format=root" will upload individually and return a digest of the root directory.
                                  "--directory-format=tree" will upload as a single tree message.
                                  "--directory-format=both" will do both. [optional]
   --output-digest-file        Write output root digest to the file in the form "<HASH>/<SIZE_BYTES>" [optional]
   --output-tree-digest-file   Write output tree digest to the file in the form "<HASH>/<SIZE_BYTES>" [optional]
   --ignore-file               Ignore files in the input directories that match the patterns in
                                  the specified file. Patterns format is as in .gitignore except
                                  ! and ** are not supported. [optional]
   --cas-server                [Deprecated] Use --remote [optional]
   --log-level                 Log level (debug, error, info, trace, warning) [optional, default = "error"]
   --verbose                   Set log level to 'debug' [optional]
                               POSITIONAL [required]

Uploads the given files and directories to CAS, then prints the digest
hash and size of the corresponding Directory messages.

The files are placed in CAS subdirectories corresponding to their
paths. For example, 'casupload file1.txt subdir/file2.txt' would
create a CAS directory containing file1.txt and a subdirectory called
'subdir' containing file2.txt.

The directories will be uploaded individually as merkle trees.
The merkle tree for a directory will contain all of the content
within the directory.

Example usage: casupload --remote=http://localhost:50051 foo.c foo.h
```
