#!/usr/bin/env bash
set -e

#
# Check that expected binaries are present and work (--help should exit with code 0)
#

BINARIES="buildbox-casd buildbox-fuse buildbox-run-bubblewrap
    buildbox-run-hosttools buildbox-run-oci buildbox-run-userchroot
    buildbox-worker casdownload casupload logstreamreceiver
    logstreamtail outputstreamer recc rexplorer rumbad trexe"

if [ -n "$ONLY_RECC_WITH_CLANG_SCAN_DEPS" ]; then
  BINARIES=recc
fi

if [ -n "$BUILD_TARGETS" ]; then
  BINARIES=""
fi

for bin in $BINARIES; do
  $bin --help >/dev/null
done
