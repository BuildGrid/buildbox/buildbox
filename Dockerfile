ARG INSTALL=on # See below for the description
ARG DEBIAN_RELEASE=bookworm

# ----------

FROM debian:${DEBIAN_RELEASE}-slim AS build

RUN apt-get update && apt-get install --yes --no-install-recommends \
    cmake \
    g++ \
    git \
    libbenchmark-dev \
    libfuse3-dev \
    libgmock-dev \
    libgoogle-glog-dev \
    libgrpc++-dev \
    libgtest-dev \
    libprotobuf-dev \
    libssl-dev \
    libtomlplusplus-dev \
    ninja-build \
    nlohmann-json3-dev \
    pkg-config \
    protobuf-compiler-grpc \
    uuid-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

#
# If set to 'on' then final image will contain built unit tests instead of binaries
#
ARG UNIT_TESTS=off

#
# If set to 'on' then only RECC will be built, with support for clang-scan-deps
#
ARG ONLY_RECC_WITH_CLANG_SCAN_DEPS=off

#
# If set to 'on' then hardening compilation flags will be enabled
#
ARG HARDEN=off

#
# If set to 'on' then mostly-static build will be enabled: static libraries will be preferred to dynamic
#
ARG MOSTLY_STATIC=off

#
# If set to 'on' then code coverage collection will be enabled
#
ARG COVERAGE=off

#
# Sets CMake build type
#
ARG BUILD_TYPE=RELEASE

#
# Sets the targets to build
#
ARG BUILD_TARGETS=""

#
# Sets the targets to build
#
ARG INSTALL=on

COPY . /buildbox

RUN \
    HARDEN=${HARDEN} \
    MOSTLY_STATIC=${MOSTLY_STATIC} \
    ONLY_RECC_WITH_CLANG_SCAN_DEPS=${ONLY_RECC_WITH_CLANG_SCAN_DEPS} \
    UNIT_TESTS=${UNIT_TESTS} \
    COVERAGE=${COVERAGE} \
    BUILD_TYPE=${BUILD_TYPE} \
    BUILD_TARGETS=${BUILD_TARGETS} \
    INSTALL=${INSTALL} \
    /buildbox/build-scripts/build.sh

#
# How the final container is built:
#
# If INSTALL is not enabled, then it contains Buildbox binaries and
# their runtime dependencies, and is built thus:
# - image-runtime-deps + image-install-on + image
#
# If INSTALL is enabled the image contains Buildbox checkout, build directory and all
# the dependencies needed to run unit tests, and is built thus:
# - image-runtime-deps + image-install-off + image
#

# ----------

FROM debian:${DEBIAN_RELEASE}-slim AS image-runtime-deps

#
# Runtime dependencies only. To make it easier to maintain this list
# only top-level dependencies are mentioned here (e.g. libgrpc++ depends
# on libprotobuf, so there is no need to specify exact libprotobuf package name)
#
RUN apt-get update && apt-get install --no-install-recommends --yes \
    bubblewrap \
    fuse3 \
    libgrpc++1.51 \
    libgoogle-glog0v6 \
    libssl3 \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# ----------

FROM image-runtime-deps AS image-install-on
ARG ONLY_RECC_WITH_CLANG_SCAN_DEPS=off
ARG BUILD_TARGETS=""


COPY --from=build /usr/local/bin/* /usr/local/bin/
COPY build-scripts/image-install-on.sh /tmp

RUN \
    ONLY_RECC_WITH_CLANG_SCAN_DEPS=${ONLY_RECC_WITH_CLANG_SCAN_DEPS} \
    BUILD_TARGETS=${BUILD_TARGETS} \
    /tmp/image-install-on.sh && rm /tmp/image-install-on.sh

# ----------

FROM image-runtime-deps AS image-install-off

#
# Additional packages needed for running tests
#
RUN apt-get update && apt-get install --yes --no-install-recommends \
    attr \
    cmake \
    g++ \
    libc6-dev \
    libbenchmark-dev \
    libfuse3-dev \
    libgoogle-glog-dev \
    libgrpc++-dev \
    libprotobuf-dev \
    libtomlplusplus-dev \
    protobuf-compiler \
    net-tools \
    nlohmann-json3-dev \
    socat \
    uuid-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

COPY --from=build /buildbox /buildbox

#
# install clang-tidy deps
#
ARG CLANG_TIDY_INSTALL
RUN if [ "$CLANG_TIDY_INSTALL" = "on" ] ; then \
    apt-get update && apt-get install --yes --no-install-recommends \
    clang-tidy \
    libgmock-dev \
    libgtest-dev \
    libssl-dev \
    python3.11-venv ; fi

# ----------

FROM image-install-on AS image-integration-tests

RUN apt-get update && apt-get install --no-install-recommends --yes \
    wget \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN GRPC_HEALTH_PROBE_VERSION=v0.4.37 && \
    wget --no-check-certificate -qO/bin/grpc_health_probe https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/${GRPC_HEALTH_PROBE_VERSION}/grpc_health_probe-linux-amd64 && \
    chmod +x /bin/grpc_health_probe

COPY --from=build /buildbox /buildbox
RUN mkdir -p /tmp/casd

ENTRYPOINT [ "/buildbox/integration/tests.sh" ]

# ----------

FROM image-install-${INSTALL} AS image
