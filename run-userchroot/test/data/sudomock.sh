#!/bin/sh
if [[ "$1" = "-u" ]]; then
   echo "sudo to user $2"
   shift 2
fi

exec "$@"
