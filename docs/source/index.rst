buildbox
========

**buildbox** provides a set of building blocks to execute actions conforming to the `Remote Execution API <https://github.com/bazelbuild/remote-apis/blob/master/build/bazel/remote/execution/v2/remote_execution.proto>`_, also supporting the `Remote Worker API <https://docs.google.com/document/d/1s_AzRRD2mdyktKUj2HWBn99rMg_3tcPvdjx3MPbFidU>`_.

The source code for all of the different components is available on GitLab: https://gitlab.com/BuildGrid/buildbox. It is published under the Apache License 2.0.

.. image:: _static/buildbox-arch.svg
  :alt: buildbox architecture diagram


Talks
-----
* `Buildbox World <https://docs.google.com/presentation/d/1tuqvro_Kpn1yLthqbsxImkcHaCKsGqsn2liuCwRsg7g>`_ - London Build Meetup (October 1st, 2019)
* `BuildGrid Executing Bazel and BuildStream Remotely <https://www.youtube.com/watch?v=w1ZA4Rrf91I>`_ - BazelCon 2018

Community
---------
You can find us in ``#buildbox`` on the `BuildTeamWorld Slack <https://bit.ly/2SG1amT>`_.

Discussions take place on the `BuildGrid mailing list <https://lists.buildgrid.build/cgi-bin/mailman/listinfo/buildgrid>`_.


Table of Contents
=================
.. toctree::
   :maxdepth: 1
   :caption: BuildBox Overview:

   about.rst
   installation.rst
   using.rst
   metrics.rst
   standard-outputs-streaming.rst
   resources.rst


.. toctree::
   :maxdepth: 1
   :caption: BuildBox Components:

   runners.rst
   worker.rst
   casd.rst
   buildbox-common.rst
   buildbox-tools.rst

.. toctree::
   :maxdepth: 1
   :caption: recc:

   recc.rst
   recc-installation.rst
   recc-running.rst
   recc-configuration-variables.rst
   recc-cmake-integration.rst
   recc-additional-utilities.rst

.. toctree::
  :maxdepth: 1
  :caption: Developing BuildBox:

  contributing.rst
  runner-dev.rst
  committers.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
