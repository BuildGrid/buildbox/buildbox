.. _worker:

Worker
======

https://gitlab.com/BuildGrid/buildbox/buildbox/-/master/worker

The **worker** is a `Remote Workers API <https://docs.google.com/document/d/1s_AzRRD2mdyktKUj2HWBn99rMg_3tcPvdjx3MPbFidU>`_ **bot**. It is responsible for connecting to a remote execution server (such as `BuildGrid <https://gitlab.com/BuildGrid/buildgrid)>`_ and obtaining work. Once it receives work to do, a worker will spawn a :ref:`runner <runners>` to carry out the work.

After the runner is finished, the worker will read an `ActionResult``, which will then be forwarded to the remote execution server that requested the work.
