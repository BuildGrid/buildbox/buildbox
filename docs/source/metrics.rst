.. _metrics:

StatsD Metrics
==============

buildbox-worker and buildbox-casd  support collecting and publishing metrics that allow to measure and quantify their work. Those values follow the `StatsD <https://github.com/statsd/statsd>`_ format.

Enabling metric collection
--------------------------
Both components have the following options:

*  ``--metrics-mode=MODE``

  Options for MODE are:
      * ``"udp://localhost:50051"``
      * ``"file:///tmp"``
      * ``"stderr"``

  Only one metric output mode can be specified.



*  ``--metrics-publish-interval=VALUE``

  Publish metric at the specified interval rate in seconds, defaults 15 seconds



Values published
----------------
For a list of the metrics that are collected and published, as well as the identifying name for each value (StatsD `bucket`), see the following source files:

- `buildboxcasd_metricnames.cpp <https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/casd/buildboxcasd_metricnames.cpp>`_
- `buildboxworker_metricnames.cpp <https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/worker/buildboxworker_metricnames.cpp>`_


Code instrumentation
--------------------
The different metric types and publishers are implemented in `buildboxcommon::buildboxcommonmetrics <https://gitlab.com/BuildGrid/buildbox/buildbox/-/tree/master/buildbox-common/buildboxcommonmetrics>`_. It also provides convenience functions that allow to easily instrument code without disrupting its flow. The documentation for that can be found `here <https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/buildbox-common/buildboxcommonmetrics/README.md>`_.
