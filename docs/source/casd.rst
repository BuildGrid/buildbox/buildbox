.. _casd:

casd
====

https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/casd/

**casd** is a local CAS caching proxy that seeks to improve performance when running multiple build jobs.

It uses a filesystem-backed storage for its cache, and has a configurable expiry mechanism that keeps disk usage bounded.

Additionaly, it supports the `LocalCas protocol <https://gitlab.com/BuildGrid/buildbox/buildbox/blob/master/protos/build/buildgrid/local_cas.proto>`_, which, among other features, allows to materialize directories via different methods.

For more information, view the `casd.md <https://gitlab.com/BuildGrid/buildbox/buildbox/-/blob/master/casd/docs/casd.md>`_ document.
