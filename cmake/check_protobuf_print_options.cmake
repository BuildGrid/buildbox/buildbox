#
# protobuf maintainers are cool.
#
# Remove once Protobuf < 0.27 is not supported
#
# 15 Jun 2015 e96ff30120a3834f7d1e31e43e591bf7cfbd731f adds option always_print_primitive_fields
#  1 Feb 2024 671b61b523dbec8bf8e41a61d5a8f689c3e2bb9f adds option always_print_without_presence_fields and deprecates always_print_primitive_fields
#
# Version 0.26.1 is released.
#
# 4 days later (that's a quite lengthy transition period):
#  5 Feb 2024 7d43131a0a3a2a7640208849eeac016c6c61374e renames always_print_fields_with_no_presence to always_print_without_presence_fields
#  5 Feb 2024 26995798757fbfef5cf6648610848e389db1fecf removes option always_print_primitive_fields
#
# Version 0.27 is released.
#
set(CMAKE_REQUIRED_LIBRARIES ${PROTOBUF_TARGET})
check_cxx_source_compiles("\
#include <google/protobuf/json/json.h>
int main() {
  google::protobuf::json::PrintOptions opts;
  opts.always_print_fields_with_no_presence = true;
}
" PROTOBUF_HAS_PRINT_OPTIONS_ALWAYS_PRINT_FIELDS_WITH_NO_PRESENCE)
check_cxx_source_compiles("\
#include <google/protobuf/json/json.h>
int main() {
  google::protobuf::json::PrintOptions opts;
  opts.always_print_without_presence_fields = true;
}
" PROTOBUF_HAS_PRINT_OPTIONS_ALWAYS_PRINT_WITHOUT_PRESENCE_FIELDS)

add_compile_definitions(PROTOBUF_HAS_PRINT_OPTIONS_ALWAYS_PRINT_FIELDS_WITH_NO_PRESENCE=$<BOOL:${PROTOBUF_HAS_PRINT_OPTIONS_ALWAYS_PRINT_FIELDS_WITH_NO_PRESENCE}>)
add_compile_definitions(PROTOBUF_HAS_PRINT_OPTIONS_ALWAYS_PRINT_WITHOUT_PRESENCE_FIELDS=$<BOOL:${PROTOBUF_HAS_PRINT_OPTIONS_ALWAYS_PRINT_WITHOUT_PRESENCE_FIELDS}>)
