# rumbad

## About

`rumbad` is a daemon that listens on a socket and publishes metadata pertaining to compilation commands to a configurable UDP port on localhost using protobuf. It then publishes the data to a persistent storage backend. Currently, `rumbad` only supports writing the data to standard output.

## Usage

```
rumbad <PORT>
```

The port is a UDP port on localhost. Typically, port `19111`.

## Configuration

`rumbad` reads the following environment variables as configurable parameters:

- `RUMBAD_RECV_BUFFER_SIZE`: Sets the value of `SO_RCVBUF` on the socket `rumbad` listens to.
- `RUMBAD_PUBLISH_INTERVAL`: The number of seconds to wait between writing batches of messages to the persistent storage.
