include(${CMAKE_SOURCE_DIR}/cmake/BuildboxGTestSetup.cmake)

add_executable(rumbad_unit_tests
  rumbad_queue.t.cpp
  rumbad_bufferedpublisher.t.cpp
  ../rumbad_bufferedpublisher.cpp
  ../rumbad_compositewriter.cpp
  ../rumbad_filewriter.cpp
  ../rumbad_statsmanager.cpp
  ../rumbad_stdoutwriter.cpp
  ../rumbad_utils.cpp
)

add_dependencies(rumbad_unit_tests rumbad)

target_precompile_headers(rumbad_unit_tests REUSE_FROM common)
target_include_directories(rumbad_unit_tests PRIVATE "..")
target_link_libraries(rumbad_unit_tests PRIVATE common ${GTEST_MAIN_TARGET} ${GMOCK_TARGET})

# To see XML output, run `GTEST_OUTPUT=xml:xunit/ ctest`
include(GoogleTest)
gtest_discover_tests("rumbad_unit_tests")
